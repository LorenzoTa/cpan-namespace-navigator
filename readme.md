First relased at [perlmonks.org](https://www.perlmonks.org/?node_id=1108341) back in 2014 where it received also strong critics (wisely), the first version now in [/V1](./V1) folder is a 100 lines of perl code with the biggest string eval ever seen in perl history.

Infact the whole `02packages.details.txt` (the file also used by cpan clients) after the download is string evaluated and it is `~21Mb` :)

String evaluation is one of worst thing a serious perl programmer can see. But is super powerful. My hubris was so great that I defended my idea and even now I'm proud of it.

That said I'm developping a wiser version.
