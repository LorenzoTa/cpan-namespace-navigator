use strict;
use warnings;
use 5.010;
use JSON;
use Data::Dump;


#!perl
use strict;
use warnings;
use Data::Dump::Streamer;
# if you wont to modify CPANnn take in consideration using Data::Dump::Streamer on the $cpan hasref
#
# UserAgent and cpan file handle. need to be here before BEGIN block,the file handle for cpan data too
my ( $ua, $cpanfh );

	my %cpan;

# BEGIN block needed to set some ENV variables
# and to evaluate LWP::UserAgent support
# Also check some contions and set the file handle $cpanfh
# and, eventually the LWP::UserAgent object $ua
# BEGIN {
    # # # WARNING !! string eval in action!!
    # # # let people to quit
    # # print "\n\nWARNING: $0 uses string eval!\n"
        # # ."Use at your own risk!\nENTER to continue or CTRL-C to terminate.\n";
    # # while (<STDIN>){last if $_ }
    
    # local $@;
    # # force Term::ReadLine to load the Term::ReadLine::Perl if present
    # $ENV{PERL_RL} = "Perl";
    # # TAB completion made possible on win32 via Term::Readline with TERM=
    # $ENV{TERM}    = 'not dumb' if $^O eq 'MSWin32';
    # # evaluate optional LWP::UserAgent support
    # eval { require LWP::UserAgent; };
    # if ($@) { print "WARNING: no LWP::UserAgent support!" }
    # # die if no LWP::UA nor filename given as arg
    # if ( $@ and !$ARGV[0] ) {
        # die "FATAL: no filename as argument nor LWP::UserAgent support!\n";
    # }
    # # let's proceed
    # $ua = LWP::UserAgent->new;
    # # this must go inside BEGIN or assignment is not run
    # my $filename =
      # defined $ARGV[0]
      # ? $ARGV[0]
      # : '02packages.details.txt';
    # # if we are here we have LWP support
    # # so if no filename was given as arg we download it
    # if ( !$ARGV[0] ) {
        # print "Downloading $filename, please wait..\n";
        # $ua->get( 'http://www.cpan.org/modules/' . $filename,
            # ':content_file' => $filename );
    # }
    # # open the file (given or downloaded)
    # # and set the filehandle
    # open $cpanfh, '<', $filename
      # or die "FATAL: unable to open '$filename' for reading!\n";
# }
use Term::ReadLine;
my $term = Term::ReadLine->new('CPAN namespace navigator');
# the main cpan hasref, container of all namespaces
$cpan{ 'CPAN' } = {} ;
# regex used to skip secret hash keys: . .. + ++
my $skiprx = qr/^[\.\+]{1,2}$/;
# used to divide in screenfulls the readme files
my $pagination = 20;
# infos about the file and help too
my @infos      = "\nINFO:\n\n";


# now feed @infos with headers from file 02packages.details.txt
# fetching the cpan file until we reach an empty line
# because after that strat namespaces enumeration



#while (<$cpanfh>) {
# while (<DATA>) {
    # print "Processing data, please wait..\n" and last if /^$/;
    # push @infos, $_;
# }
# push @infos, $_
  # for "\n\n",
  # "USAGE: $0 [02packages.details.txt | or other valid file]\n\nNAVIGATION:\n\n",
  # ".  simple list of contained namespaces\n", ".. move one level up\n",
  # "+  detailed list of namespaces directly contained in the current one\n",
  # "++ dump a simple recursive tree of contained namespaces\n",
  # "*  read the readme file of current namespace; needs LWP::UserAgent\n",
  # "** download the current namespace's package; needs LWP::UserAgent\n",
  # "?  print this help\n",
  # "\nTAB completion, case insensitive, enabled on all sub namespaces\n",
  # "$0 by Discipulus as found at perlmonks.org\n\n";

# main extrapolation loop
# we go on fetchin the cpan file
# because now there are only namespaces
#while (<$cpanfh>) {
while (<DATA>) {
    # AA::BB::CC  0.01 D/DI/DISCIPULUS/AA-BB-CC-0.001.tar.gz
    chomp;
    # split namespaces, version, partial path
    my @fields = split /\s+/;
    # split namespace in AA BB CC
    my @names     = split /::/, $fields[0];
    # die if received invalid data              # or is better /\.gz|zip|tgz|bz2$/ ?
    unless (defined $names[0] and $fields[2]=~ /^[A-Z]{1}\/[A-Z]{2}\/[A-Z]+/ ) {
          die "FATAL: no valid data in the file?\nReceived: $_"
              . join ' ',@fields
              ."\n";
    }
    # sanitize names containing ' that seems to valid
    map {s/'/\\'/} @names;
    # @ancestors are @names less last element
    my @ancestors = @names;
    pop @ancestors;
    
    
	#my $package = ( split /\s+/, $_ )[0];
		my $cur = \$cpan{'CPAN'}; #print "\$cur = ";dd $cur;
		#foreach my $ele ( split '::',$package ){
		foreach my $ele ( @names ){
			#print "\tprocessing [$ele]\n";
			$$cur->{ $ele } = {} unless exists $$cur->{ $ele };
			$cur = \$$cur->{ $ele };
		}
	
}

#dd %cpan;
use Data::Dumper;
print Dumper \%cpan;

__DATA__
AAA::eBay                         undef  J/JW/JWACH/Apache-FastForward-1.1.tar.gz
AAAA::Crypt::DH                    0.06  B/BI/BINGOS/AAAA-Crypt-DH-0.06.tar.gz
AAAA::Mail::SpamAssassin          0.002  S/SC/SCHWIGON/AAAA-Mail-SpamAssassin-0.002.tar.gz
AAAAAAAAA                          1.01  M/MS/MSCHWERN/AAAAAAAAA-1.01.tar.gz
AAC::Pvoice                        0.91  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::Bitmap                1.12  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::Dialog                1.01  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::EditableRow           1.04  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::Input                 1.12  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::Panel                 1.15  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
AAC::Pvoice::Row                   1.05  J/JO/JOUKE/AAC-Pvoice-0.91.tar.gz
Aard                              0.001  M/MG/MGV/Aard-0.001.tar.gz
abbreviation                       0.02  M/MI/MIYAGAWA/abbreviation-0.02.tar.gz
ABI                                 1.0  M/MA/MALAY/ABI-1.0.tar.gz
Abilities                           0.5  I/ID/IDOPEREL/Abilities-0.5.tar.gz
Abilities::Features                 0.5  I/ID/IDOPEREL/Abilities-0.5.tar.gz
Abilities::Scoped                   0.2  I/ID/IDOPEREL/Abilities-0.2.tar.gz
ABNF::Generator                   undef  N/NY/NYAAPA/ABNF-Grammar-0.08.tar.gz
ABNF::Generator::Honest           undef  N/NY/NYAAPA/ABNF-Grammar-0.08.tar.gz
ABNF::Generator::Liar             undef  N/NY/NYAAPA/ABNF-Grammar-0.08.tar.gz
ABNF::Grammar                      0.08  N/NY/NYAAPA/ABNF-Grammar-0.08.tar.gz
ABNF::Validator                   undef  N/NY/NYAAPA/ABNF-Grammar-0.08.tar.gz
